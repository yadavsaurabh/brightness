all:
	cp brightness.py brightness
	chmod +x brightness

install:
	cp brightness.py /usr/local/bin/brightness
	chmod +x /usr/local/bin/brightness

uninstall:
	rm -f /usr/local/bin/brightness

