#!/usr/bin/env python3

# This is a simple program for controlling brightness.
# Created by : Saurabh Kumar Yadav
# Email : yadavsaurabh@pm.me
# GIT Repository : https://gitlab.com/yadavsaurabh
# For license view LICENSE file distributed with this file.

import sys
import os

arguments = sys.argv
arguments = arguments[1:]
pass_on = None

def help_sec():
    print('Usage:')
    output = ["--set [number] : Sets brightess to [number] percent. (Don't use % with number)",
              "--dec [number] : Decreases [number] percent brightness. (Don't use % with number)",
              "--dec [number] : Increases [number] percent brightness. (Don't use % with number)",
              "--cur : Shows current brightness level.",
              "--help : Shows this section."]
    for i in range(len(output)):
        print(' ' * 6, end = '')
        print(output[i])

driver_name = next(os.walk('/sys/class/backlight'))[1]
driver_name = driver_name[0]
cur_brightness_file = open('/sys/class/backlight/' + driver_name + '/brightness', 'r').read()
length = len(cur_brightness_file)
cur_brightness_file = int(cur_brightness_file[:-1])
max_brightness = open('/sys/class/backlight/' + driver_name + '/max_brightness', 'r').read()
max_brightness = int(max_brightness[:-1])

if (arguments == []):
    print('Invalid input.\n')
    help_sec()

elif (arguments[0] == '--cur'):
    cur_brightness = cur_brightness_file / (max_brightness / 100)
    print ('Current brightness level : %0.f' % cur_brightness + '%')

elif (arguments[0] == '--set'):
    if ((len(arguments) != 2)):
        print('Invalid input.\n')
        help_sec()
        pass_on = False
    else:
        if (arguments[1].isdigit() and int(arguments[1]) in range(101)):
            print('Setting brightness...')
            pass_on = True
        else:
            print('Invalid input.\n')
            help_sec()
            pass_on = False
    if (pass_on):
        cur_brightness_file = open('/sys/class/backlight/' + driver_name + '/brightness', 'w')
        future_brightness = int(int(arguments[1]) * (max_brightness / 100))
        cur_brightness_file.write(str(future_brightness) + '\n')

elif (arguments[0] == '--inc'):
    if ((len(arguments) != 2)):
        print('Invalid input.\n')
        help_sec()
        pass_on = False
    else:
        if (arguments[1].isdigit() == False):
            print('Invalid input.\n')
            help_sec()
            pass_on = False
        else:
            future_brightness = cur_brightness_file + int((int(arguments[1]) * (max_brightness / 100)))
            if (future_brightness in range(max_brightness + 1)):
                print('Setting brightness...')
                pass_on = True
            else:
                print('Invalid input.\n')
                help_sec()
                pass_on = False

    if (pass_on):
        cur_brightness_file = open('/sys/class/backlight/' + driver_name + '/brightness', 'w')
        cur_brightness_file.write(str(future_brightness) + '\n')

elif (arguments[0] == '--dec'):
    if ((len(arguments) != 2)):
        print('Invalid input.\n')
        help_sec()
        pass_on = False
    else:
        if (arguments[1].isdigit() == False):
            print('Invalid input.\n')
            help_sec()
            pass_on = False
        else:
            future_brightness = cur_brightness_file - int((int(arguments[1]) * (max_brightness / 100)))
            if (future_brightness in range(max_brightness + 1)):
                print('Setting brightness...')
                pass_on = True
            else:
                print('Invalid input.\n')
                help_sec()
                pass_on = False

    if (pass_on):
        cur_brightness_file = open('/sys/class/backlight/' + driver_name + '/brightness', 'w')
        cur_brightness_file.write(str(future_brightness) + '\n')

elif (arguments[0] == '--help'):
    help_sec()

else:
    print('Invalid input.\n')
    help_sec()
